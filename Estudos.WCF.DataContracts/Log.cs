﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace Estudos.WCF.DataContracts
{
    [DataContract(Namespace = "http://Estudos/data/log")]
    public class Log
    {      
        [DataMember(Order =0)]
        public string Aplicacao { get; set; }
        [DataMember(Order =1)]
        public string Mensagem { get; set; }        
        public DateTime DataOcorrencia { get; set; }
        public string TipoOcorrencia { get; set; }
    }
}
