﻿namespace Estudos.WCF.Servico
{
    partial class ProjectInstaller1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        /// 
        private System.ComponentModel.IContainer components = null;
        private System.ServiceProcess.ServiceInstaller Instalador;
        private System.ServiceProcess.ServiceProcessInstaller ProcessoInstalador;


        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.Instalador = new System.ServiceProcess.ServiceInstaller();
            this.ProcessoInstalador = new System.ServiceProcess.ServiceProcessInstaller();

            this.Instalador.Description = "Estudos";
            this.Instalador.DisplayName = "Instalando um serviço";
            this.Instalador.ServiceName = "Estudos";
            this.Instalador.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.Instalador.DelayedAutoStart = true;

            this.ProcessoInstalador.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ProcessoInstalador.Password = null;
            this.ProcessoInstalador.Username = null;

            this.Installers.AddRange(new System.Configuration.Install.Installer[]
            {
                this.ProcessoInstalador,this.Instalador
            });
        }

        #endregion
    }
}