﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Estudos.WCF.DataContracts;
using Estudos.WCF.Services;
using System;
using System.Threading;

namespace Console.Estudos.Hosting
{
    class Program
    {
        public static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(LogUtilityService)))
            {
                host.Open();
                System.Console.ReadKey();
            }
        }
    }
}
