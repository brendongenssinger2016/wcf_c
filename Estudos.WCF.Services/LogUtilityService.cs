﻿using Estudos.WCF.DataContracts;
using Estudos.WCF.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Estudos.WCF.Repository;

namespace Estudos.WCF.Services
{
    public class LogUtilityService : ILogUtilityServices
    {
        private void ProcessarRegistro(Log log,string tipoDeOcorrencia)
        {
            if (String.IsNullOrEmpty(tipoDeOcorrencia))
                throw new FaultException("não informado");
            log.DataOcorrencia = DateTime.Now;
            log.TipoOcorrencia = tipoDeOcorrencia;
            new LogRepository().InlcuirRegistroLog(log);
        }
        public void RegistrarAlerta(Log log)
        {
            ProcessarRegistro(log, "ALT");
        }

        public void RegistrarErro(Log log)
        {
            ProcessarRegistro(log, "ALT");
        }

        public void RegistrarEvento(Log log)
        {
            ProcessarRegistro(log, "EVT");
        }
    }
}
