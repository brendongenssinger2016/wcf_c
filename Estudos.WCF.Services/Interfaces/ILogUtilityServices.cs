﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Estudos.WCF.DataContracts;

namespace Estudos.WCF.Services.Interfaces
{
    [ServiceContract(Namespace = "http://TesteWcf_TCP/services/log")]
    public interface ILogUtilityServices
    {
        [OperationContract]
        void RegistrarErro(Log log);
        [OperationContract]
        void RegistrarAlerta(Log log);
        [OperationContract]
        void RegistrarEvento(Log log);        
    }
}
