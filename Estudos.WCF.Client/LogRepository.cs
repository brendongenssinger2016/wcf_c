﻿using Estudos.WCF.DataContracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudos.WCF.Repository
{
    public class LogRepository
    {
        public void InlcuirRegistroLog(Log log)
        {
            var escreva = new StreamWriter("log.txt", true);
            escreva.WriteLine(log.Aplicacao);
            escreva.WriteLine(log.DataOcorrencia);
            escreva.WriteLine(log.Mensagem);
            escreva.WriteLine(log.TipoOcorrencia);            
            escreva.Close();
        }
    }
}
