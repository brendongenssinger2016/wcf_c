﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TesteWCF_TCP.ServicoWCFTCP;
namespace TesteWCF_TCP
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            using (LogUtilityServicesClient cliente = new LogUtilityServicesClient())
            {
                Log log = new Log();
                log.Aplicacao = "Brendon";
                log.Mensagem = "Primeiro WCF";
                cliente.RegistrarAlerta(log);
            }
        }
    }
}
